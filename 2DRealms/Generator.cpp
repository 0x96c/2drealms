#include "Generator.h"

Generator::Generator(unsigned int height)
	: gen_(rd_()), dis_(0, height)
{}

int Generator::Generate()
{
	return dis_(gen_);
}