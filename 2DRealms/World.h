#ifndef WORLD_H
#define WORLD_H

#include <random>
#include <vector>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "Generator.h"
#include "Island.h"

class World : public sf::Drawable
{
public:
	World(const sf::Vector2i& scale, float gravity);

	bool IsCollision(const sf::FloatRect& object);

	float& GetGravity();
	void SetGravity(float gravity);
private:
	float gravity_;

	sf::Texture texture_;

	std::vector<Island> islands_;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif // WORLD_H
