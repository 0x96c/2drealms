#include "Island.h"

Island::Island(Generator& gen, const sf::Texture& texture)
{
	sf::Sprite block(texture);

	int width = 5;

	for (int i = 0; i <= width; ++i)
	{
		block.setPosition(600 - 32 - gen.Generate() * 32, 600 - 32 - gen.Generate() * 32);
		blocks_.push_back(block);
	}
}

sf::Vector2f& Island::GetPosition()
{
	return position_;
}

void Island::SetPosition(sf::Vector2f& position)
{
	position_ = position;
}

bool Island::IsCollision(const sf::FloatRect& object)
{
	for (sf::Sprite& block : blocks_)
	{
		if (object.intersects(block.getGlobalBounds()))
		{
			return true;
		}
	}

	return false;
}

void Island::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (const sf::Sprite& block : blocks_)
	{
		target.draw(block);
	}
}