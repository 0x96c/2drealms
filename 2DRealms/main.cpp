#include <SFML/Graphics.hpp>

#include "Player.h"
#include "World.h"

int main(void)
{
	sf::Texture player_texture;
	player_texture.loadFromFile("player.png");
	Player player(player_texture);

	World world(sf::Vector2i(800, 600), 0.2f);

	sf::RenderWindow window(sf::VideoMode(800, 600), "2D Realms");
	window.setFramerateLimit(80);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) 
			{
				window.close();
			}
		}

		player.Handle();

		player.Update(world);

		window.clear();
		window.draw(world);
		window.draw(player);
		window.display();
	}

	return 0;
}