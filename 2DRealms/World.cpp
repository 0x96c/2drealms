#include "World.h"

World::World(const sf::Vector2i& scale, float gravity)
{
	gravity_ = gravity;

	Generator gen(2);

	texture_.loadFromFile("tile.png");
	Island island(gen, texture_);

	for (int x = 0; x <= scale.x; ++x)
	{
		island.SetPosition(sf::Vector2f(gen.Generate(), gen.Generate()));
		islands_.push_back(island);
	}
}

float& World::GetGravity()
{
	return gravity_;
}

void World::SetGravity(float gravity)
{
	gravity_ = gravity;
}

bool World::IsCollision(const sf::FloatRect& object)
{
	for (Island& island : islands_)
	{
		if (island.IsCollision(object))
		{
			return true;
		}
	}

	return false;
}

void World::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (const Island& island : islands_)
	{
		target.draw(island);
	}
}