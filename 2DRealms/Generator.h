#ifndef GENERATOR_H
#define GENERATOR_H

#include <algorithm>
#include <numeric>
#include <random>
#include <vector>

class Generator 
{
public:
	Generator(unsigned int height);

	int Generate();
private:
	std::random_device rd_;
	std::mt19937 gen_;
	std::uniform_int_distribution<> dis_;
};

#endif // GENERATOR_H