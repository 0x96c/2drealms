#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Event.hpp>

#include "World.h"

class Player : public sf::Sprite
{
public:
	Player(const sf::Texture& texture);

	void Handle();
	void Update(World& world);
private:
	sf::Vector2f velocity_;

	bool is_jumping_;
};

#endif // PLAYER_H

