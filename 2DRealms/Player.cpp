#include "Player.h"

Player::Player(const sf::Texture& texture)
{
	this->setTexture(texture);
	this->setOrigin(this->getScale() / 2.0f);
	this->setPosition(400, 300);
	this->setScale(3, 3);
}

void Player::Handle()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		if (!is_jumping_)
		{
			velocity_.y = -4;
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		velocity_.x = -4;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		velocity_.x = 4;
	}
}

void Player::Update(World& world)
{
	this->move(velocity_.x, velocity_.y);

	if (!world.IsCollision(this->getGlobalBounds()))
	{
		velocity_.y += world.GetGravity();
		is_jumping_ = true;
	}
	else 
	{
		this->move(-velocity_.x, -velocity_.y);
		velocity_.y = 0;
		is_jumping_ = false;
	}
	
	velocity_.x = 0;
}