#pragma once
#ifndef ISLAND_H
#define ISLAND_H

#include <vector>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include "Generator.h"

class Island : public sf::Drawable
{
public:
	Island(Generator& gen, const sf::Texture& texture);

	sf::Vector2f& GetPosition();
	void SetPosition(sf::Vector2f& position);

	bool IsCollision(const sf::FloatRect& object);
private:
	std::vector<sf::Sprite> blocks_;
	sf::Vector2f position_;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif // ISLAND_H